import Home from "./Home";
import JoinRoom from "./JoinRoom";
import Game from "./Game";

const Pages = {
  Home,
  JoinRoom,
  Game,
}

export default Pages;

const ROUTES = {
  ROOT: "/",
  HOMEPAGE: "/home",
  SIGNUP: "/auth/signup",
  LOGIN: "/auth/login",
  JOINROOM: "/joinroom",
  GAME: "/game",
  PAGENOTFOUND: "/page-not-found",
};

export default ROUTES;
